
import java.awt.Color;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import org.apache.commons.validator.routines.UrlValidator;

public class ScanFrame extends javax.swing.JFrame {

    public static ScanFrame scanFrame;
    private static DefaultMutableTreeNode root_tree = new DefaultMutableTreeNode("SAP");
    private static DefaultMutableTreeNode domain_tree = null;

    private static boolean isRun = true;
    private static boolean isEnd = false;
    private static boolean isPause = false;
    private static boolean isResume = true;
    private static int endFull = 0;
    private static String selectedFrame = "nmap";
    public static int isInstall = 0;

    public static boolean nmap = false;
    public static boolean dirscan = false;
    public static boolean dnsrecon = false;
    public static boolean sslchecker = false;
    public static boolean subdomainscan = false;
    public static boolean wpscan = false;
    public static boolean tracert = false;

    static ArrayList<ResponseClass> responseClassList = new ArrayList<ResponseClass>();

    int x, y;

    public ScanFrame() {
        initComponents();

        wpscan_lb_rb.setEnabled(false);
        tracert_lb_rb.setEnabled(false);

        Main.show();

        nmap_lb_frameMouseReleased(null);
        jLabel3.setText("IP: " + getGlobalIpAdd());

        nmap_text_area1.setText("NMAP is Offline");
        dir_text_area.setText("DIR SCAN is Offline");
        dns_recon_text_area.setText("DNS RECON is Offline");
        ssl_checker_text_area.setText("SSL CHECKER is Offline");
        subdomain_text_area.setText("SUBDOMAIN SCAN is Offline");
        wpscan_text_area.setText("WPSCAN SCAN is Offline");
        tracert_text_area.setText("TRACERT is Offline");

        stop_lb.setEnabled(false);
        resume_lb.setEnabled(false);
        pause_lb.setEnabled(false);
        save_lb.setEnabled(false);
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ScanFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ScanFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ScanFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ScanFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

            }
        });

        LoadingFrame loadingFrame = new LoadingFrame();
        loadingFrame.setVisible(true);
        try {
            Thread.sleep(1000);
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/setup_complete.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            Thread.sleep(2600);
            loadingFrame.setVisible(false);
        } catch (Exception ex) {

        }
        //loadingFrame.runInstall();
        isInstall = 1;
        while (true) {
            if (isInstall == 1) {
                //Main.installWPSCAN();
                //Main.updateWPSCAN();
                //Main.loadConfig();
                //loadingFrame.setVisible(false);
                scanFrame = new ScanFrame();
                scanFrame.setVisible(true);
                /////////////////////////audio
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            AudioPlayer player = new AudioPlayer();
                            player.play(ScanFrame.class.getResource("sound/welcome.wav").getPath());
                        } catch (Exception ex) {
                        }
                    }
                });
                th.start();
                ////////////////////////
                break;
            } else if (isInstall == -1) {

                System.exit(1);
                break;
            }
        }

    }

    public static void setVisibleFrame(boolean value) {
        scanFrame.setVisible(value);
    }

    private static void startScan() {
        sap_label.setEnabled(false);
        domain_scan_frame_tf.setEnabled(false);
        nmap_lb_rb.setEnabled(false);
        dirscan_lb_rb.setEnabled(false);
        dnsrecon_lb_rb.setEnabled(false);
        sslchecker_lb_rb.setEnabled(false);
        subdomain_lb_rb.setEnabled(false);

        stop_lb.setEnabled(true);
        resume_lb.setEnabled(false);
        pause_lb.setEnabled(true);
        save_lb.setEnabled(false);

        try {
            if (nmap) {
                nmap_text_area1.setText("");
                endFull++;
                runNMAP();
            } else {
                nmap_text_area1.setText("NMAP is Offline");
            }
            if (dirscan) {
                dir_text_area.setText("");
                endFull++;
                runDIR();
            } else {
                dir_text_area.setText("DIR SCAN is Offline");
            }
            if (dnsrecon) {
                dns_recon_text_area.setText("");
                endFull++;
                runDNSRECON();
            } else {
                dns_recon_text_area.setText("DNS RECON is Offline");
            }
            if (sslchecker) {
                ssl_checker_text_area.setText("");
                endFull++;
                runSSL_CHECKER();
            } else {
                ssl_checker_text_area.setText("SSL CHECKER is Offline");
            }
            if (subdomainscan) {
                subdomain_text_area.setText("");
                endFull++;
                runSUBDOMAIN_SCAN();
            } else {
                subdomain_text_area.setText("SUBDOMAIN SCAN is Offline");
            }
            if (wpscan) {
                wpscan_text_area.setText("");
                endFull++;
                runWPSCAN();
            } else {
                wpscan_text_area.setText("WPSCAN SCAN is Offline");
            }
            if (tracert) {
                tracert_text_area.setText("");
                endFull++;
                runTRACERT();
            } else {
                tracert_text_area.setText("TRACERT is Offline");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        checkLife();
    }

    private static void checkLife() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        if (endFull == 0) {
                            stop_lb.setEnabled(false);
                            pause_lb.setEnabled(false);
                            resume_lb.setEnabled(false);
                            save_lb.setEnabled(true);
                        } else {
                            Thread.sleep(2000);
                        }
                    }
                } catch (Exception ex) {
                }
            }
        });
        th.start();
    }
//////////////////////////////////////////////////////////////////////////////////

    private static void runNMAP() {

        HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
        try ( BufferedReader br = new BufferedReader(new FileReader(ScanFrame.class.getResource("list/tcp.csv").getFile()))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                hash_map.put(Integer.valueOf(values[1]), values[2].replaceAll("\"", ""));
            }
        } catch (Exception ex) {

        }

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                String target = Main.getDomainName(domain_scan_frame_tf.getText().trim());
                //Main.runNMAP(target);
                //nmap_text_area.setText(target);

                int coreCount = Runtime.getRuntime().availableProcessors();
                ExecutorService service = Executors.newFixedThreadPool(coreCount);

                ArrayList<Future> allFutures = new ArrayList<>();

                for (int i = 1; i <= 65535; i++) {
                    int port = i;
                    Callable<String> task = () -> {
                        try {
                            while (true) {
                                if (isRun == false && isEnd == true) {
                                    //th.stop();
                                    nmap_text_area1.append("\n Scan is Stoped");
                                    return "null";
                                } else if (isRun == false && isEnd == false) {
                                    try {
                                        Thread.sleep(2000);
                                    } catch (Exception ex) {

                                    }
                                } else if (isRun == true && isEnd == false) {
                                    //th.resume();
                                    return String.valueOf(port) + ":" + String.valueOf(Main.checkTCPport(port, target));
                                }
                            }

                        } catch (Exception ex) {
                            return "null";
                        }
                    };
                    Future<String> future = service.submit(task);
                    allFutures.add(future);
                }

                for (int i = 0; i < 65535; i++) {
                    Future<String> future = allFutures.get(i);
                    try {
                        String result = future.get();
                        String[] coup = result.split(":"); //0-port 1-value
                        nmap_text_area1.append("TCP   " + coup[0] + "   " + coup[1] + "   " + hash_map.get(Integer.valueOf(coup[0])) + "\n");///////////////////////////////////////////////////
                        if (Boolean.valueOf(coup[1])) {
                            Object data[] = {"TCP", Integer.valueOf(coup[0]), Boolean.valueOf(coup[1]), hash_map.get(Integer.valueOf(coup[0]))};
                            DefaultTableModel tblModel = (DefaultTableModel) nmapTable.getModel();
                            tblModel.addRow(data);
                        }
                    } catch (Exception ex) {

                    }
                }

                endFull--;
            }
        });
        th.start();
    }
/////////////////////////////////////////////////////////////////

    public static DefaultMutableTreeNode searchNode(String nodeStr, DefaultMutableTreeNode nod) {
        DefaultMutableTreeNode node = null;
        Enumeration e = nod.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            node = (DefaultMutableTreeNode) e.nextElement();
            NodeDir nd = (NodeDir) node.getUserObject();
            if (nodeStr.equals(nd.getPath())) {
                return node;
            }
        }
        return null;
    }

    private static void runDIR() {
        //1ci etapda 5defe folder_name.txt axtarir, 5cinden sonra son 2sini qarshilashdiri, eger olculeri eynidise stop eleyir
        //eyni deilse davam edir.

        //2ci ve 3ci etapda vulns.txt faylindakilari axtarir, tapdiqimiz folderlerde, eyni zamanda folder_name.txt faylindaki folder namelerin
        //sonluquna extensions.txt faylindakilar elave olunur ve axtarilir.
        //elave: ancaq 404 leri gosterme qalanlarini qrafik shekilde eks etdir ver yaninda statusunu yaz.
        //numune olaraq: test.php 200 34231(olcusu)
        try {
            ArrayList<String> path = new ArrayList<String>();
            File myFile = new File(ScanFrame.class.getResource("list/folder_name.txt").getFile());
            System.out.println(myFile.getAbsolutePath());
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                path.add(data);
            }
            myReader.close();

            ArrayList<String> ext = new ArrayList<String>();
            File myFileExt = new File(ScanFrame.class.getResource("list/extensions.txt").getFile());
            System.out.println(myFileExt.getAbsolutePath());
            Scanner myReaderExt = new Scanner(myFileExt);
            while (myReaderExt.hasNextLine()) {
                String data = myReaderExt.nextLine();
                ext.add(data);
            }
            myReaderExt.close();

            ArrayList<String> vul = new ArrayList<String>();
            File myFileVul = new File(ScanFrame.class.getResource("list/vulns.txt").getFile());
            System.out.println(myFileVul.getAbsolutePath());
            Scanner myReaderVul = new Scanner(myFileVul);
            while (myReaderVul.hasNextLine()) {
                String data = myReaderVul.nextLine();
                vul.add(data);
            }
            myReaderVul.close();
            //path.subList(1, 2)

            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    ArrayList<String> allPath = new ArrayList<String>();
                    allPath.add("/");
                    int iteratorNum = 0;

                    DefaultTreeModel model = new DefaultTreeModel(root_tree);
                    dir_tree.setModel(model);
                    String target = domain_scan_frame_tf.getText().trim();
                    target = Main.getDomainName(target);
                    NodeDir mainNodeDir = new NodeDir();
                    mainNodeDir.setPath(target);
                    domain_tree = new DefaultMutableTreeNode(mainNodeDir);
                    root_tree.add(domain_tree);
                    model.reload();

                    //////////////////////////////////////////////google
                    Set<String> googleResult = Main.googleSearchResult("site:" + target);
                    System.out.println(googleResult.size());
                    for (String r : googleResult) {
                        try {
                            String path = new URL(r).getPath();
                            //allPath.add(path);
                            path = path.replaceFirst("/", "");
                            //path = path.substring(1,path.length()-1);
                            String[] paths = path.split("/");
                            DefaultMutableTreeNode n = null;
                            String supAddPath = "";
                            for (int path_i = 0; path_i < paths.length; path_i++) {
                                supAddPath += paths[path_i] + "/";
                                if (!supAddPath.contains(".") && (!supAddPath.trim().equals("") || supAddPath.trim() != null)) {
                                    if (!allPath.contains(supAddPath.trim())) {
                                        allPath.add(supAddPath.trim());
                                    }
                                }
                                if (path_i == 0) {
                                    n = searchNode(paths[path_i], domain_tree);
                                    if (n == null) {
                                        NodeDir nd = new NodeDir();
                                        nd.setPath(paths[path_i]);
                                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                        n = newNode;
                                        domain_tree.add(n);
                                        model.reload();
                                    } else {

                                    }
                                } else {
                                    DefaultMutableTreeNode nn = searchNode(paths[path_i], n);
                                    if (nn == null) {
                                        NodeDir nd = new NodeDir();
                                        if (paths.length - 1 == path_i) {
                                            nd.setUrl(r);
                                            nd.setCode("code: 200");
                                            nd.setSize("Google search");
                                            System.out.println(r);
                                        }
                                        nd.setPath(paths[path_i]);

                                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                        n.add(newNode);
                                        //domain_tree.add(n);
                                        n = newNode;
                                        model.reload();
                                    } else {
                                        n = nn;
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    ///////////////////////////////////////////////////google end
                    //for (String s : allPath) {
                    //    System.out.println(s);
                    //    System.out.println(allPath.size());
                    //}

                    int coreCount = Runtime.getRuntime().availableProcessors();
                    ExecutorService service = Executors.newFixedThreadPool(coreCount);

                    ArrayList<Future> allFutures = new ArrayList<>();

                    for (int i = 0; i < vul.size(); i++) {
                        int index = i;

                        Callable<String> task = () -> {
                            try {
                                while (true) {
                                    if (isRun == false && isEnd == true) {
                                        //th.stop();
                                        dir_text_area.append("\n Scan is Stoped");
                                        return null;
                                    } else if (isRun == false && isEnd == false) {
                                        try {
                                            Thread.sleep(2000);
                                        } catch (Exception ex) {

                                        }
                                    } else if (isRun == true && isEnd == false) {
                                        //th.resume();

                                        //return String.valueOf(port) + ":" + String.valueOf(Main.checkTCPport(port, target));
                                        ResponseClass responseClass = DirectoryScanner.requestUrl(domain_scan_frame_tf.getText().trim(), vul.get(index));
                                        dir_text_area.append(responseClass.toString() + "\n");

                                        if (responseClass.getCode() == 200 || (responseClass.getCode() >= 300 && responseClass.getCode() < 400) || responseClass.getCode() == 403) {
                                            boolean sizeEq = false;
                                            for (ResponseClass res : responseClassList) {
                                                if (res.getSize() == responseClass.getSize() && res.getCode() == responseClass.getCode()) {
                                                    sizeEq = true;
                                                    break;
                                                }
                                            }
                                            responseClassList.add(responseClass);
                                            if (!sizeEq) {
                                                String path = responseClass.getUrl().getPath();
                                                if (!path.contains(".") && (!path.trim().equals("") || path.trim() != null)) {
                                                    if (!allPath.contains(path.trim())) {
                                                        allPath.add(path.trim());
                                                    }
                                                }
                                                path = path.replaceFirst("/", "");
                                                while (true) {
                                                    if (path.substring(0, 1).equals("/")) {
                                                        try {
                                                            path = path.substring(1, path.length() - 1);
                                                        } catch (Exception ex) {
                                                            break;
                                                        }
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                String[] paths = path.split("/");
                                                DefaultMutableTreeNode n = null;
                                                for (int path_i = 0; path_i < paths.length; path_i++) {
                                                    if (path_i == 0) {
                                                        n = searchNode(paths[path_i], domain_tree);
                                                        if (n == null) {
                                                            NodeDir nd = new NodeDir();
                                                            nd.setPath(paths[path_i]);
                                                            System.out.println(paths[path_i]);
                                                            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                                            n = newNode;
                                                            domain_tree.add(n);
                                                            model.reload();
                                                        } else {

                                                        }
                                                    } else {
                                                        DefaultMutableTreeNode nn = searchNode(paths[path_i], n);
                                                        if (nn == null) {
                                                            NodeDir nd = new NodeDir();
                                                            if (paths.length - 1 == path_i) {
                                                                nd.setCode(String.valueOf(responseClass.getCode()));
                                                                nd.setSize(String.valueOf(responseClass.getSize()));
                                                                nd.setUrl(responseClass.getUrl().toString());
                                                            }
                                                            nd.setPath(paths[path_i]);
                                                            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                                            n.add(newNode);
                                                            //domain_tree.add(n);
                                                            n = newNode;
                                                            model.reload();
                                                        } else {
                                                            n = nn;
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        //System.out.println("Complete 1");
                                        return "Complete";
                                    }
                                }

                            } catch (Exception ex) {
                                return "null";
                            }
                        };
                        Future<String> future = service.submit(task);
                        allFutures.add(future);
                    }

                    for (int i = 0; i < vul.size(); i++) {
                        Future<String> future = allFutures.get(i);
                        try {
                            String result = future.get();
                            //String[] coup = result.split(":"); //0-port 1-value
                            //dir_text_area.append(result + "\n");
                        } catch (Exception ex) {

                        }
                    }

                    /////////////////////////////
                    coreCount = Runtime.getRuntime().availableProcessors();
                    service = Executors.newFixedThreadPool(coreCount);

                    allFutures = new ArrayList<>();

                    for (int j = 0; j < 10000; j++) {
                        int index_j = j;
                        if (j < allPath.size()) {

                        } else {
                            break;
                        }

                        for (int i = 0; i < path.size(); i++) {
                            int index_i = i;
                            Callable<String> task = () -> {
                                try {
                                    while (true) {
                                        if (isRun == false && isEnd == true) {
                                            //th.stop();
                                            //nmap_text_area1.append("\n Scan is Stoped");
                                            return null;
                                        } else if (isRun == false && isEnd == false) {
                                            try {
                                                Thread.sleep(2000);
                                            } catch (Exception ex) {

                                            }
                                        } else if (isRun == true && isEnd == false) {
                                            //th.resume();
                                            ResponseClass responseClass = DirectoryScanner.requestUrl(domain_scan_frame_tf.getText().trim(), allPath.get(index_j) + path.get(index_i));
                                            dir_text_area.append(responseClass.toString() + "\n");

                                            if (responseClass.getCode() == 200 || (responseClass.getCode() >= 300 && responseClass.getCode() < 400) || responseClass.getCode() == 403) {
                                                boolean sizeEq = false;
                                                for (ResponseClass res : responseClassList) {
                                                    if (res.getSize() == responseClass.getSize() && res.getCode() == responseClass.getCode()) {
                                                        sizeEq = true;
                                                        break;
                                                    }
                                                }
                                                responseClassList.add(responseClass);

                                                if (!sizeEq) {
                                                    String path = responseClass.getUrl().getPath();
                                                    if (!path.contains(".") && (!path.trim().equals("") || path.trim() != null)) {
                                                        if (!allPath.contains(path.trim())) {
                                                            allPath.add(path.trim());
                                                        }
                                                    }
                                                    path = path.replaceFirst("/", "");
                                                    while (true) {
                                                        if (path.substring(0, 1).equals("/")) {
                                                            path = path.substring(1, path.length() - 1);
                                                        } else {
                                                            break;
                                                        }
                                                    }
                                                    String[] paths = path.split("/");
                                                    DefaultMutableTreeNode n = null;
                                                    for (int path_i = 0; path_i < paths.length; path_i++) {
                                                        if (path_i == 0) {
                                                            n = searchNode(paths[path_i], domain_tree);
                                                            if (n == null) {
                                                                NodeDir nd = new NodeDir();
                                                                nd.setPath(paths[path_i]);
                                                                System.out.println(paths[path_i]);
                                                                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                                                n = newNode;
                                                                domain_tree.add(n);
                                                                model.reload();
                                                            } else {

                                                            }
                                                        } else {
                                                            DefaultMutableTreeNode nn = searchNode(paths[path_i], n);
                                                            if (nn == null) {
                                                                NodeDir nd = new NodeDir();
                                                                if (paths.length - 1 == path_i) {
                                                                    nd.setCode(String.valueOf(responseClass.getCode()));
                                                                    nd.setSize(String.valueOf(responseClass.getSize()));
                                                                    nd.setUrl(responseClass.getUrl().toString());
                                                                }
                                                                nd.setPath(paths[path_i]);
                                                                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode();
                                                                n.add(newNode);
                                                                //domain_tree.add(n);
                                                                n = newNode;
                                                                model.reload();
                                                            } else {
                                                                n = nn;
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                            //
                                            //System.out.println("Complete 2");
                                            //return String.valueOf(port) + ":" + String.valueOf(Main.checkTCPport(port, target));

                                            return "Complete";
                                        }

                                    }

                                } catch (Exception ex) {
                                    return "null";
                                }
                            };
                            Future<String> future = service.submit(task);
                            allFutures.add(future);
                            ///////////////////
                        }
                        for (int i = 0; i < path.size(); i++) {
                            Future<String> future = allFutures.get(i);
                            try {
                                //String result = future.get();
                                //String[] coup = result.split(":"); //0-port 1-value
                                //nmap_text_area1.append("TCP   " + coup[0] + "   " + coup[1] + "   " + hash_map.get(Integer.valueOf(coup[0])) + "\n");///////////////////////////////////////////////////
                            } catch (Exception ex) {

                            }
                        }
                    }
                    ///////////////////////
                    //for (String s : allPath) {
                    //  System.out.println(s);
                    //System.out.println(allPath.size());
                    //}
                    for (int h = 0; h < allPath.size(); h++) {
                        int index_h = h;
                        for (int j = 0; j < path.size(); j++) {
                            int index_j = j;
                            for (int i = 0; i < ext.size(); i++) {
                                int index_i = i;
                                Callable<String> task = () -> {
                                    try {
                                        while (true) {
                                            if (isRun == false && isEnd == true) {
                                                //th.stop();
                                                //nmap_text_area1.append("\n Scan is Stoped");
                                                return null;
                                            } else if (isRun == false && isEnd == false) {
                                                try {
                                                    Thread.sleep(2000);
                                                } catch (Exception ex) {

                                                }
                                            } else if (isRun == true && isEnd == false) {
                                                //th.resume();
                                                //return String.valueOf(port) + ":" + String.valueOf(Main.checkTCPport(port, target));
                                                ResponseClass responseClass = DirectoryScanner.requestUrl(domain_scan_frame_tf.getText().trim(), allPath.get(index_h) + path.get(index_j) + ext.get(index_i));
                                                dir_text_area.append(responseClass.toString() + "\n");

                                                if (responseClass.getCode() == 200 || (responseClass.getCode() >= 300 && responseClass.getCode() < 400) || responseClass.getCode() == 403) {
                                                    boolean sizeEq = false;
                                                    for (ResponseClass res : responseClassList) {
                                                        if (res.getSize() == responseClass.getSize() && res.getCode() == responseClass.getCode()) {
                                                            sizeEq = true;
                                                            break;
                                                        }
                                                    }
                                                    responseClassList.add(responseClass);

                                                    if (!sizeEq) {
                                                        String path = responseClass.getUrl().getPath();
                                                        if (!path.contains(".") && (!path.trim().equals("") || path.trim() != null)) {
                                                            if (!allPath.contains(path.trim())) {
                                                                allPath.add(path.trim());
                                                            }
                                                        }
                                                        path = path.replaceFirst("/", "");
                                                        while (true) {
                                                            if (path.substring(0, 1).equals("/")) {
                                                                path = path.substring(1, path.length() - 1);
                                                            } else {
                                                                break;
                                                            }
                                                        }
                                                        String[] paths = path.split("/");
                                                        DefaultMutableTreeNode n = null;
                                                        for (int path_i = 0; path_i < paths.length; path_i++) {
                                                            if (path_i == 0) {
                                                                n = searchNode(paths[path_i], domain_tree);
                                                                if (n == null) {
                                                                    NodeDir nd = new NodeDir();
                                                                    nd.setPath(paths[path_i]);
                                                                    System.out.println(paths[path_i]);
                                                                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                                                    n = newNode;
                                                                    domain_tree.add(n);
                                                                    model.reload();
                                                                } else {

                                                                }
                                                            } else {
                                                                DefaultMutableTreeNode nn = searchNode(paths[path_i], n);
                                                                if (nn == null) {
                                                                    NodeDir nd = new NodeDir();
                                                                    if (paths.length - 1 == path_i) {
                                                                        nd.setCode(String.valueOf(responseClass.getCode()));
                                                                        nd.setSize(String.valueOf(responseClass.getSize()));
                                                                        nd.setUrl(responseClass.getUrl().toString());
                                                                    }
                                                                    nd.setPath(paths[path_i]);
                                                                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nd);
                                                                    n.add(newNode);
                                                                    //domain_tree.add(n);
                                                                    n = newNode;
                                                                    model.reload();
                                                                } else {
                                                                    n = nn;
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                                //System.out.println("Complete 3");
                                                return "Complete";
                                            }

                                        }

                                    } catch (Exception ex) {
                                        return "null";
                                    }
                                };
                                Future<String> future = service.submit(task);
                                allFutures.add(future);
                            }
                            for (int jj = 0; jj < path.size(); jj++) {
                                Future<String> future = allFutures.get(jj);
                                try {
                                    String result = future.get();
                                    //String[] coup = result.split(":"); //0-port 1-value
                                    //nmap_text_area1.append("TCP   " + coup[0] + "   " + coup[1] + "   " + hash_map.get(Integer.valueOf(coup[0])) + "\n");///////////////////////////////////////////////////
                                } catch (Exception ex) {

                                }
                            }
                        }
                    }
                    endFull--;
                }
            });
            th.start();

        } catch (Exception ex) {

        }
    }

    private static void runDNSRECON() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                String target = domain_scan_frame_tf.getText().trim();
                target = Main.getDomainName(target);
                //Main.runDNSRECON(target);
                //dns_recon_text_area.setText(target);

                String host = target;
                try {
                    InetAddress inetAddress = InetAddress.getByName(host);
                    // show the Internet Address as name/address
                    dns_recon_text_area.append("\n" + inetAddress.getHostName() + " " + inetAddress.getHostAddress() + "\n\n");
                    System.out.println(inetAddress.getHostName() + " " + inetAddress.getHostAddress());

                    Hashtable<String, String> env = new Hashtable<String, String>();
                    //env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
                    //env.put(Context.PROVIDER_URL, "ldap://localhost:389/o=JNDITutorial");

                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
                    //env.put(Context.PROVIDER_URL, "dns://google.com");

                    // get the default initial Directory Context
                    InitialDirContext iDirC = new InitialDirContext(env);
                    // get the DNS records for inetAddress
                    Attributes attributes = iDirC.getAttributes("dns:/" + inetAddress.getHostName());
                    // get an enumeration of the attributes and print them out
                    NamingEnumeration<?> attributeEnumeration = (NamingEnumeration<?>) attributes.getAll();
                    //System.out.println("");
                    while (attributeEnumeration.hasMore()) {
                        dns_recon_text_area.append("" + attributeEnumeration.next() + "\n");
                        //System.out.println("" + attributeEnumeration.next());
                    }
                    attributeEnumeration.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                endFull--;
            }
        });
        th.start();
    }

    private static void runSSL_CHECKER() {
        Thread th;
        th = new Thread(new Runnable() {
            @Override
            public void run() {
                String target = domain_scan_frame_tf.getText().trim();
                //target = Main.getDomainName(target);
                //Main.runSSL_CHECKER(target);
                //ssl_checker_text_area.setText(target);
                try {
                    URL destinationURL = new URL(target);
                    HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection();
                    conn.connect();
                    Certificate[] certs = (Certificate[]) conn.getServerCertificates();
                    for (Certificate cert : certs) {
                        System.out.println("Certificate is: " + cert);
                        if (cert instanceof X509Certificate) {
                            X509Certificate x = (X509Certificate) cert;
                            System.out.println(x.getIssuerDN());
                            //ssl_checker_text_area.append("\n" + x.getIssuerDN()+ "\n");
                            ssl_checker_text_area.append("\n" + x.toString() + "\n");
                        }
                    }
                } catch (Exception ex) {

                }
                endFull--;
            }
        });
        th.start();
    }

    public static String requestUrlSubDoamin(String domain) {
        int statusCode = -1;
        try {

            if (true) {
                URL url = new URL("http://" + domain);
                HttpURLConnection con = null;
                con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                statusCode = con.getResponseCode();
                con.setInstanceFollowRedirects(false);
                if (statusCode == 200 || statusCode == 301 || statusCode == 403) {
                    return "true:" + statusCode;
                }
            }
            if (true) {
                URL url = new URL("https://" + domain);
                HttpsURLConnection con = null;
                con = (HttpsURLConnection) url.openConnection();
                con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                statusCode = con.getResponseCode();
                con.setInstanceFollowRedirects(false);
                if (statusCode == 200 || statusCode == 301 || statusCode == 403) {
                    return "true:" + statusCode;
                }
            }
        } catch (Exception ex) {

        }
        return "false:" + -1;
    }

    private static void runSUBDOMAIN_SCAN() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> path = new ArrayList<String>();
                try {
                    File myFile = new File(ScanFrame.class.getResource("list/folder_name.txt").getFile());
                    System.out.println(myFile.getAbsolutePath());
                    Scanner myReader = new Scanner(myFile);
                    while (myReader.hasNextLine()) {
                        String data = myReader.nextLine();
                        path.add(data);
                    }
                    myReader.close();
                } catch (Exception ex) {

                }
                final String target = Main.getDomainName(domain_scan_frame_tf.getText().trim());

                //Main.runSUBDOMAIN_SCAN(target);
                //dns_recon_text_area.setText(target);
                int coreCount = Runtime.getRuntime().availableProcessors();
                ExecutorService service = Executors.newFixedThreadPool(coreCount);

                ArrayList<Future> allFutures = new ArrayList<>();

                for (int i = 0; i < path.size(); i++) {
                    int index = i;
                    Callable<String> task = () -> {
                        try {
                            while (true) {
                                if (isRun == false && isEnd == true) {
                                    //th.stop();
                                    subdomain_text_area.append("\n Scan is Stoped");
                                    return "null";
                                } else if (isRun == false && isEnd == false) {
                                    try {
                                        Thread.sleep(2000);
                                    } catch (Exception ex) {

                                    }
                                } else if (isRun == true && isEnd == false) {
                                    //th.resume();
                                    //return String.valueOf(port) + ":" + String.valueOf(Main.checkTCPport(port, target));
                                    String[] result = requestUrlSubDoamin(path.get(index) + "." + target).split(":");
                                    synchronized (subdomain_text_area) {
                                        subdomain_text_area.append(path.get(index) + "." + target + "\n");
                                    }
                                    if (Boolean.valueOf(result[0])) {
                                        Object data[] = {path.get(index), Boolean.valueOf(result[0]), Integer.valueOf(result[1])};
                                        DefaultTableModel tblModel = (DefaultTableModel) subdomainTable.getModel();
                                        tblModel.addRow(data);
                                    }
                                    return "null";
                                }
                            }

                        } catch (Exception ex) {
                            return "null";
                        }
                    };
                    Future<String> future = service.submit(task);
                    allFutures.add(future);
                }

                for (int i = 0; i < path.size(); i++) {
                    Future<String> future = allFutures.get(i);
                    try {
                        String result = future.get();
                    } catch (Exception ex) {

                    }
                }
                endFull--;
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        });
        th.start();
    }

    private static void runWPSCAN() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                String target = domain_scan_frame_tf.getText().trim();
                target = Main.getDomainName(target);
                Main.runWPSCAN(target);
                //dir_text_area.setText("\n"+target);
                endFull--;
            }
        });
        th.start();
        Thread th2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (isRun == false && isEnd == true) {
                        th.stop();
                        wpscan_text_area.append("\n Scan is Stoped");
                        break;
                    } else if (isRun == false && isEnd == false && isResume == false) {
                        //th.suspend();
                        isPause = true;
                        isResume = true;
                    } else if (isRun == true && isEnd == false && isPause == true) {
                        //th.resume();
                        isPause = false;
                    } else {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception ex) {

                        }
                    }
                }
            }
        });
        th2.start();
    }

    private static void runTRACERT() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                String target = domain_scan_frame_tf.getText().trim();
                target = Main.getDomainName(target);
                Main.runTRACERT(target);
                //dir_text_area.setText("\n"+target);
                endFull--;
            }
        });
        th.start();
        Thread th2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (isRun == false && isEnd == true) {
                        th.stop();
                        tracert_text_area.append("\n Scan is Stoped");
                        break;
                    } else if (isRun == false && isEnd == false && isResume == false) {
                        //th.suspend();
                        isPause = true;
                        isResume = true;
                    } else if (isRun == true && isEnd == false && isPause == true) {
                        //th.resume();
                        isPause = false;
                    } else {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception ex) {

                        }
                    }
                }
            }
        });
        th2.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel8 = new javax.swing.JPanel();
        dir_lb_frame = new javax.swing.JPanel();
        nmap_lb_frame = new javax.swing.JLabel();
        dir_lb_frame1 = new javax.swing.JLabel();
        dns_lb_frame = new javax.swing.JLabel();
        ssl_lb_frame = new javax.swing.JLabel();
        subdomain_lb_frame = new javax.swing.JLabel();
        wpscan_lb_frame = new javax.swing.JLabel();
        tracert_lb_frame = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        domain_scan_frame_tf = new javax.swing.JTextField();
        sap_label = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        nmap_lb_rb = new javax.swing.JLabel();
        dirscan_lb_rb = new javax.swing.JLabel();
        dnsrecon_lb_rb = new javax.swing.JLabel();
        sslchecker_lb_rb = new javax.swing.JLabel();
        subdomain_lb_rb = new javax.swing.JLabel();
        wpscan_lb_rb = new javax.swing.JLabel();
        tracert_lb_rb = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        exit_label = new javax.swing.JLabel();
        hide_label = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        restart_label = new javax.swing.JLabel();
        result_lay_panel = new javax.swing.JLayeredPane();
        nmap_panel1 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        nmap_text_area1 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        nmapTable = new javax.swing.JTable();
        dir_panel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        dir_text_area = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        dir_tree = new javax.swing.JTree();
        dnsrecon_panel = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        dns_recon_text_area = new javax.swing.JTextArea();
        ssl_checker_panel = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        ssl_checker_text_area = new javax.swing.JTextArea();
        subdomain_panel = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        subdomain_text_area = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        subdomainTable = new javax.swing.JTable();
        wpscan_panel = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        wpscan_text_area = new javax.swing.JTextArea();
        tracert_panel = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tracert_text_area = new javax.swing.JTextArea();
        jPanel10 = new javax.swing.JPanel();
        stop_lb = new javax.swing.JLabel();
        pause_lb = new javax.swing.JLabel();
        resume_lb = new javax.swing.JLabel();
        save_lb = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        dir_lb_frame.setBackground(new java.awt.Color(120, 212, 247));

        nmap_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        nmap_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        nmap_lb_frame.setText(" NMAP");
        nmap_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        nmap_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nmap_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                nmap_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                nmap_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                nmap_lb_frameMouseReleased(evt);
            }
        });

        dir_lb_frame1.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        dir_lb_frame1.setForeground(new java.awt.Color(255, 255, 255));
        dir_lb_frame1.setText(" DIR");
        dir_lb_frame1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dir_lb_frame1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dir_lb_frame1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dir_lb_frame1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dir_lb_frame1MouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                dir_lb_frame1MouseReleased(evt);
            }
        });

        dns_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        dns_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        dns_lb_frame.setText(" DNS");
        dns_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dns_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dns_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dns_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dns_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                dns_lb_frameMouseReleased(evt);
            }
        });

        ssl_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        ssl_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        ssl_lb_frame.setText(" SSL");
        ssl_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ssl_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ssl_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ssl_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ssl_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                ssl_lb_frameMouseReleased(evt);
            }
        });

        subdomain_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        subdomain_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        subdomain_lb_frame.setText(" SUBDOMAIN");
        subdomain_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        subdomain_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                subdomain_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                subdomain_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                subdomain_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                subdomain_lb_frameMouseReleased(evt);
            }
        });

        wpscan_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        wpscan_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        wpscan_lb_frame.setText(" WPSCAN");
        wpscan_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wpscan_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wpscan_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                wpscan_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                wpscan_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                wpscan_lb_frameMouseReleased(evt);
            }
        });

        tracert_lb_frame.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        tracert_lb_frame.setForeground(new java.awt.Color(255, 255, 255));
        tracert_lb_frame.setText(" TRACERT");
        tracert_lb_frame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tracert_lb_frame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tracert_lb_frameMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tracert_lb_frameMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tracert_lb_frameMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tracert_lb_frameMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout dir_lb_frameLayout = new javax.swing.GroupLayout(dir_lb_frame);
        dir_lb_frame.setLayout(dir_lb_frameLayout);
        dir_lb_frameLayout.setHorizontalGroup(
            dir_lb_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nmap_lb_frame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(dir_lb_frame1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(dns_lb_frame, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(ssl_lb_frame, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(subdomain_lb_frame, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
            .addComponent(wpscan_lb_frame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(tracert_lb_frame, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
        );
        dir_lb_frameLayout.setVerticalGroup(
            dir_lb_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dir_lb_frameLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(nmap_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(dir_lb_frame1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(dns_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ssl_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(subdomain_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(wpscan_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(tracert_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(292, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 156, 207));
        jPanel2.setAlignmentX(0.0F);

        jPanel3.setBackground(new java.awt.Color(0, 156, 207));

        domain_scan_frame_tf.setFont(new java.awt.Font("Calibri", 0, 13)); // NOI18N
        domain_scan_frame_tf.setToolTipText("");
        domain_scan_frame_tf.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(83, 86, 90), 5));
        domain_scan_frame_tf.setDragEnabled(true);

        sap_label.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        sap_label.setForeground(new java.awt.Color(255, 255, 255));
        sap_label.setText("  S.A.P.  ");
        sap_label.setAlignmentY(0.0F);
        sap_label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(83, 86, 90), 5));
        sap_label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sap_label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sap_labelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                sap_labelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                sap_labelMouseExited(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(83, 86, 90));
        jLabel1.setOpaque(true);

        jLabel2.setBackground(new java.awt.Color(83, 86, 90));
        jLabel2.setOpaque(true);

        jPanel9.setBackground(new java.awt.Color(0, 156, 207));
        jPanel9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        nmap_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        nmap_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        nmap_lb_rb.setText(" NMAP ");
        nmap_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        nmap_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        nmap_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nmap_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                nmap_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                nmap_lb_rbMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nmap_lb_rbMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                nmap_lb_rbMouseReleased(evt);
            }
        });

        dirscan_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        dirscan_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        dirscan_lb_rb.setText(" DIRSCAN ");
        dirscan_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        dirscan_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dirscan_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dirscan_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dirscan_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dirscan_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                dirscan_lb_rbMouseReleased(evt);
            }
        });

        dnsrecon_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        dnsrecon_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        dnsrecon_lb_rb.setText(" DNSRECON ");
        dnsrecon_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        dnsrecon_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dnsrecon_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dnsrecon_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dnsrecon_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dnsrecon_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                dnsrecon_lb_rbMouseReleased(evt);
            }
        });

        sslchecker_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        sslchecker_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        sslchecker_lb_rb.setText(" SSLCHECKER ");
        sslchecker_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        sslchecker_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sslchecker_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sslchecker_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                sslchecker_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                sslchecker_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sslchecker_lb_rbMouseReleased(evt);
            }
        });

        subdomain_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        subdomain_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        subdomain_lb_rb.setText(" SUBDOMAIN ");
        subdomain_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        subdomain_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        subdomain_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                subdomain_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                subdomain_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                subdomain_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                subdomain_lb_rbMouseReleased(evt);
            }
        });

        wpscan_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        wpscan_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        wpscan_lb_rb.setText(" WPSCAN ");
        wpscan_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        wpscan_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wpscan_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wpscan_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                wpscan_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                wpscan_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                wpscan_lb_rbMouseReleased(evt);
            }
        });

        tracert_lb_rb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 18)); // NOI18N
        tracert_lb_rb.setForeground(new java.awt.Color(255, 255, 255));
        tracert_lb_rb.setText(" TRACERT ");
        tracert_lb_rb.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, java.awt.Color.white), "OFF", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Gill Sans Ultra Bold", 0, 13), new java.awt.Color(255, 51, 51))); // NOI18N
        tracert_lb_rb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tracert_lb_rb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tracert_lb_rbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tracert_lb_rbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tracert_lb_rbMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tracert_lb_rbMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(wpscan_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(subdomain_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sslchecker_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(dnsrecon_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dirscan_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nmap_lb_rb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tracert_lb_rb, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
                .addContainerGap(263, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(nmap_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dirscan_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dnsrecon_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sslchecker_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(subdomain_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(wpscan_lb_rb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tracert_lb_rb)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(sap_label))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(domain_scan_frame_tf, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(domain_scan_frame_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(sap_label, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(83, 86, 90));
        jPanel7.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel7MouseDragged(evt);
            }
        });
        jPanel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel7MousePressed(evt);
            }
        });

        exit_label.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        exit_label.setForeground(new java.awt.Color(255, 255, 255));
        exit_label.setText(" EXIT ");
        exit_label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        exit_label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exit_labelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exit_labelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                exit_labelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                exit_labelMousePressed(evt);
            }
        });

        hide_label.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        hide_label.setForeground(new java.awt.Color(255, 255, 255));
        hide_label.setText(" HIDE ");
        hide_label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        hide_label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                hide_labelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                hide_labelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hide_labelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                hide_labelMousePressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("S.A.P.     v0.5");

        restart_label.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        restart_label.setForeground(new java.awt.Color(255, 255, 255));
        restart_label.setText("RESTART");
        restart_label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        restart_label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                restart_labelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                restart_labelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                restart_labelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                restart_labelMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1204, Short.MAX_VALUE)
                .addComponent(restart_label)
                .addGap(56, 56, 56)
                .addComponent(hide_label)
                .addGap(18, 18, 18)
                .addComponent(exit_label)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exit_label, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
                    .addComponent(hide_label, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
                    .addComponent(jLabel4)
                    .addComponent(restart_label, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
                .addContainerGap())
        );

        result_lay_panel.setBackground(new java.awt.Color(204, 255, 255));
        result_lay_panel.setForeground(new java.awt.Color(204, 255, 255));
        result_lay_panel.setOpaque(true);

        nmap_panel1.setBackground(new java.awt.Color(255, 255, 255));
        nmap_panel1.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane10.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane10.setBorder(null);

        nmap_text_area1.setEditable(false);
        nmap_text_area1.setColumns(20);
        nmap_text_area1.setRows(5);
        nmap_text_area1.setText("adadadda");
        nmap_text_area1.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane10.setViewportView(nmap_text_area1);

        nmapTable.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        nmapTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Type", "Port", "Open", "Desciption"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Boolean.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(nmapTable);
        if (nmapTable.getColumnModel().getColumnCount() > 0) {
            nmapTable.getColumnModel().getColumn(2).setHeaderValue("Open");
            nmapTable.getColumnModel().getColumn(3).setResizable(false);
            nmapTable.getColumnModel().getColumn(3).setHeaderValue("Desciption");
        }

        javax.swing.GroupLayout nmap_panel1Layout = new javax.swing.GroupLayout(nmap_panel1);
        nmap_panel1.setLayout(nmap_panel1Layout);
        nmap_panel1Layout.setHorizontalGroup(
            nmap_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
            .addGroup(nmap_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(nmap_panel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        nmap_panel1Layout.setVerticalGroup(
            nmap_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nmap_panel1Layout.createSequentialGroup()
                .addGap(0, 444, Short.MAX_VALUE)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(nmap_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(nmap_panel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(142, Short.MAX_VALUE)))
        );

        dir_panel.setBackground(new java.awt.Color(255, 255, 255));
        dir_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane4.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane4.setBorder(null);

        dir_text_area.setEditable(false);
        dir_text_area.setColumns(20);
        dir_text_area.setRows(5);
        dir_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane4.setViewportView(dir_text_area);

        dir_tree.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        dir_tree.setForeground(new java.awt.Color(83, 86, 90));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("SAP");
        dir_tree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        dir_tree.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane2.setViewportView(dir_tree);

        javax.swing.GroupLayout dir_panelLayout = new javax.swing.GroupLayout(dir_panel);
        dir_panel.setLayout(dir_panelLayout);
        dir_panelLayout.setHorizontalGroup(
            dir_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
            .addGroup(dir_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(dir_panelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        dir_panelLayout.setVerticalGroup(
            dir_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dir_panelLayout.createSequentialGroup()
                .addGap(0, 449, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(dir_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(dir_panelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(110, Short.MAX_VALUE)))
        );

        dnsrecon_panel.setBackground(new java.awt.Color(255, 255, 255));
        dnsrecon_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane5.setBorder(null);
        jScrollPane5.setForeground(new java.awt.Color(255, 255, 255));

        dns_recon_text_area.setEditable(false);
        dns_recon_text_area.setColumns(20);
        dns_recon_text_area.setRows(5);
        dns_recon_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane5.setViewportView(dns_recon_text_area);

        javax.swing.GroupLayout dnsrecon_panelLayout = new javax.swing.GroupLayout(dnsrecon_panel);
        dnsrecon_panel.setLayout(dnsrecon_panelLayout);
        dnsrecon_panelLayout.setHorizontalGroup(
            dnsrecon_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
        );
        dnsrecon_panelLayout.setVerticalGroup(
            dnsrecon_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
        );

        ssl_checker_panel.setBackground(new java.awt.Color(255, 255, 255));
        ssl_checker_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);

        ssl_checker_text_area.setEditable(false);
        ssl_checker_text_area.setColumns(20);
        ssl_checker_text_area.setRows(5);
        ssl_checker_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane6.setViewportView(ssl_checker_text_area);

        javax.swing.GroupLayout ssl_checker_panelLayout = new javax.swing.GroupLayout(ssl_checker_panel);
        ssl_checker_panel.setLayout(ssl_checker_panelLayout);
        ssl_checker_panelLayout.setHorizontalGroup(
            ssl_checker_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
        );
        ssl_checker_panelLayout.setVerticalGroup(
            ssl_checker_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
        );

        subdomain_panel.setBackground(new java.awt.Color(255, 255, 255));
        subdomain_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane7.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane7.setBorder(null);

        subdomain_text_area.setEditable(false);
        subdomain_text_area.setColumns(20);
        subdomain_text_area.setRows(5);
        subdomain_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane7.setViewportView(subdomain_text_area);

        subdomainTable.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        subdomainTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Domain", "Open", "Status Code"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(subdomainTable);

        javax.swing.GroupLayout subdomain_panelLayout = new javax.swing.GroupLayout(subdomain_panel);
        subdomain_panel.setLayout(subdomain_panelLayout);
        subdomain_panelLayout.setHorizontalGroup(
            subdomain_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
            .addGroup(subdomain_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(subdomain_panelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        subdomain_panelLayout.setVerticalGroup(
            subdomain_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, subdomain_panelLayout.createSequentialGroup()
                .addGap(0, 446, Short.MAX_VALUE)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(subdomain_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(subdomain_panelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(154, Short.MAX_VALUE)))
        );

        wpscan_panel.setBackground(new java.awt.Color(255, 255, 255));
        wpscan_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane8.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane8.setBorder(null);

        wpscan_text_area.setEditable(false);
        wpscan_text_area.setColumns(20);
        wpscan_text_area.setRows(5);
        wpscan_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane8.setViewportView(wpscan_text_area);

        javax.swing.GroupLayout wpscan_panelLayout = new javax.swing.GroupLayout(wpscan_panel);
        wpscan_panel.setLayout(wpscan_panelLayout);
        wpscan_panelLayout.setHorizontalGroup(
            wpscan_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(wpscan_panelLayout.createSequentialGroup()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
                .addContainerGap())
        );
        wpscan_panelLayout.setVerticalGroup(
            wpscan_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
        );

        tracert_panel.setBackground(new java.awt.Color(255, 255, 255));
        tracert_panel.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane9.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane9.setBorder(null);

        tracert_text_area.setEditable(false);
        tracert_text_area.setColumns(20);
        tracert_text_area.setRows(5);
        tracert_text_area.setSelectionColor(new java.awt.Color(83, 86, 90));
        jScrollPane9.setViewportView(tracert_text_area);

        javax.swing.GroupLayout tracert_panelLayout = new javax.swing.GroupLayout(tracert_panel);
        tracert_panel.setLayout(tracert_panelLayout);
        tracert_panelLayout.setHorizontalGroup(
            tracert_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
        );
        tracert_panelLayout.setVerticalGroup(
            tracert_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
        );

        result_lay_panel.setLayer(nmap_panel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(dir_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(dnsrecon_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(ssl_checker_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(subdomain_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(wpscan_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        result_lay_panel.setLayer(tracert_panel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout result_lay_panelLayout = new javax.swing.GroupLayout(result_lay_panel);
        result_lay_panel.setLayout(result_lay_panelLayout);
        result_lay_panelLayout.setHorizontalGroup(
            result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 817, Short.MAX_VALUE)
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(nmap_panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(dir_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(dnsrecon_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(ssl_checker_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(subdomain_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(wpscan_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(tracert_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        result_lay_panelLayout.setVerticalGroup(
            result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 548, Short.MAX_VALUE)
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(nmap_panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(dir_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(dnsrecon_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(ssl_checker_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(subdomain_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(wpscan_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(result_lay_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(tracert_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1495, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(dir_lb_frame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(result_lay_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 637, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(dir_lb_frame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(result_lay_panel))
                    .addGap(0, 0, 0)))
        );

        jPanel10.setBackground(new java.awt.Color(83, 86, 90));

        stop_lb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        stop_lb.setForeground(new java.awt.Color(255, 255, 255));
        stop_lb.setText(" STOP ");
        stop_lb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        stop_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stop_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                stop_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                stop_lbMouseExited(evt);
            }
        });

        pause_lb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        pause_lb.setForeground(new java.awt.Color(255, 255, 255));
        pause_lb.setText(" PAUSE ");
        pause_lb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pause_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pause_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pause_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pause_lbMouseExited(evt);
            }
        });

        resume_lb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        resume_lb.setForeground(new java.awt.Color(255, 255, 255));
        resume_lb.setText(" RESUME ");
        resume_lb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        resume_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resume_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                resume_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                resume_lbMouseExited(evt);
            }
        });

        save_lb.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        save_lb.setForeground(new java.awt.Color(255, 255, 255));
        save_lb.setText(" SAVE ");
        save_lb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                save_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save_lbMouseExited(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 13)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("IP");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(763, 763, 763)
                .addComponent(save_lb)
                .addGap(18, 18, 18)
                .addComponent(resume_lb)
                .addGap(18, 18, 18)
                .addComponent(pause_lb)
                .addGap(18, 18, 18)
                .addComponent(stop_lb)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stop_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pause_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(resume_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(save_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void exit_labelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exit_labelMouseClicked
        int yes_no = JOptionPane.showConfirmDialog(this, "Do you want to EXIT ?", "Confirm", JOptionPane.YES_NO_OPTION);
        if (yes_no == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_exit_labelMouseClicked

    private void exit_labelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exit_labelMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_exit_labelMousePressed

    private void jPanel7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_jPanel7MousePressed

    private void jPanel7MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseDragged
        int xx = evt.getXOnScreen();
        int yy = evt.getYOnScreen();
        this.setLocation(xx - x, yy - y);
    }//GEN-LAST:event_jPanel7MouseDragged

    private void exit_labelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exit_labelMouseEntered
        exit_label.setBackground(new Color(255, 255, 255));
        exit_label.setForeground(new Color(83, 86, 90));
        exit_label.setOpaque(true);
    }//GEN-LAST:event_exit_labelMouseEntered

    private void exit_labelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exit_labelMouseExited
        exit_label.setBackground(new Color(83, 86, 90));
        exit_label.setForeground(new Color(255, 255, 255));
        exit_label.setOpaque(true);
    }//GEN-LAST:event_exit_labelMouseExited

    public static boolean urlValidator(String url) {
        // Get an UrlValidator using default schemes
        UrlValidator defaultValidator = new UrlValidator();
        return defaultValidator.isValid(url);
    }

    private void sap_labelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sap_labelMouseClicked
        if (sap_label.isEnabled()) {

            if (urlValidator(domain_scan_frame_tf.getText().trim())) {
                sap_label.setBackground(new Color(0, 156, 207));
                sap_label.setForeground(new Color(255, 255, 255));
                sap_label.setOpaque(true);
                isRun = true;
                isEnd = false;
                startScan();
            } else {
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            AudioPlayer player = new AudioPlayer();
                            player.play(ScanFrame.class.getResource("sound/repeat_2.wav").getPath());
                        } catch (Exception ex) {
                        }
                    }
                });
                th.start();
            }
        }
    }//GEN-LAST:event_sap_labelMouseClicked

    private void sap_labelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sap_labelMouseEntered
        if (sap_label.isEnabled()) {
            sap_label.setBackground(new Color(255, 255, 255));
            sap_label.setForeground(new Color(0, 156, 207));
            sap_label.setOpaque(true);
        }
    }//GEN-LAST:event_sap_labelMouseEntered

    private void sap_labelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sap_labelMouseExited
        sap_label.setBackground(new Color(0, 156, 207));
        sap_label.setForeground(new Color(255, 255, 255));
        sap_label.setOpaque(true);
    }//GEN-LAST:event_sap_labelMouseExited

    private void stop_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stop_lbMouseClicked
        if (stop_lb.isEnabled()) {
            isRun = false;
            isEnd = true;
            stop_lb.setEnabled(false);
            save_lb.setEnabled(true);
            pause_lb.setEnabled(false);
            resume_lb.setEnabled(false);
        }
    }//GEN-LAST:event_stop_lbMouseClicked

    private void stop_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stop_lbMouseEntered
        if (stop_lb.isEnabled()) {
            stop_lb.setBackground(new Color(255, 255, 255));
            stop_lb.setForeground(new Color(83, 86, 90));
            stop_lb.setOpaque(true);
        }
    }//GEN-LAST:event_stop_lbMouseEntered

    private void stop_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stop_lbMouseExited
        stop_lb.setBackground(new Color(83, 86, 90));
        stop_lb.setForeground(new Color(255, 255, 255));
        stop_lb.setOpaque(true);
    }//GEN-LAST:event_stop_lbMouseExited

    private void pause_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pause_lbMouseClicked
        if (pause_lb.isEnabled()) {
            isRun = false;
            isEnd = false;
            pause_lb.setEnabled(false);
            resume_lb.setEnabled(true);
            //isResume = false;
        }
    }//GEN-LAST:event_pause_lbMouseClicked

    private void resume_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resume_lbMouseClicked
        if (resume_lb.isEnabled()) {
            isRun = true;
            isEnd = false;
            pause_lb.setEnabled(true);
            resume_lb.setEnabled(false);
        }
    }//GEN-LAST:event_resume_lbMouseClicked

    private void pause_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pause_lbMouseEntered
        if (pause_lb.isEnabled()) {
            pause_lb.setBackground(new Color(255, 255, 255));
            pause_lb.setForeground(new Color(83, 86, 90));
            pause_lb.setOpaque(true);
        }
    }//GEN-LAST:event_pause_lbMouseEntered

    private void pause_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pause_lbMouseExited
        pause_lb.setBackground(new Color(83, 86, 90));
        pause_lb.setForeground(new Color(255, 255, 255));
        pause_lb.setOpaque(true);
    }//GEN-LAST:event_pause_lbMouseExited

    private void resume_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resume_lbMouseEntered
        if (resume_lb.isEnabled()) {
            resume_lb.setBackground(new Color(255, 255, 255));
            resume_lb.setForeground(new Color(83, 86, 90));
            resume_lb.setOpaque(true);
        }
    }//GEN-LAST:event_resume_lbMouseEntered

    private void resume_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resume_lbMouseExited
        resume_lb.setBackground(new Color(83, 86, 90));
        resume_lb.setForeground(new Color(255, 255, 255));
        resume_lb.setOpaque(true);
    }//GEN-LAST:event_resume_lbMouseExited

    private void save_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_save_lbMouseClicked
        try {
            JFileChooser jc = new JFileChooser();
            jc.showSaveDialog(this);
            File f = jc.getSelectedFile();
            FileWriter fw = new FileWriter(f);
            String report = "-------------------------NMAP-----------------------------\n"
                    + nmap_text_area1.getText() + "\n\n\n"
                    + "-------------------------DIR SCAN-----------------------------\n"
                    + dir_text_area.getText() + "\n\n\n"
                    + "-------------------------DNS RECON-----------------------------\n"
                    + dns_recon_text_area.getText() + "\n\n\n"
                    + "-------------------------SSL CHECKER-----------------------------\n"
                    + ssl_checker_text_area.getText() + "\n\n\n"
                    + "-------------------------SUBDOMAIN SCAN-----------------------------\n"
                    + subdomain_text_area.getText() + "\n\n\n"
                    + "-------------------------WP SCAN-----------------------------\n"
                    + wpscan_text_area.getText() + "\n\n\n"
                    + "-------------------------TRACERT-----------------------------\n"
                    + tracert_text_area.getText();
            fw.write(report);
            fw.close();
        } catch (Exception ex) {

        }
    }//GEN-LAST:event_save_lbMouseClicked

    private void save_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_save_lbMouseEntered
        if (save_lb.isEnabled()) {
            save_lb.setBackground(new Color(255, 255, 255));
            save_lb.setForeground(new Color(83, 86, 90));
            save_lb.setOpaque(true);
        }
    }//GEN-LAST:event_save_lbMouseEntered

    private void save_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_save_lbMouseExited
        save_lb.setBackground(new Color(83, 86, 90));
        save_lb.setForeground(new Color(255, 255, 255));
        save_lb.setOpaque(true);
    }//GEN-LAST:event_save_lbMouseExited

    private void nmap_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_frameMouseClicked

    }//GEN-LAST:event_nmap_lb_frameMouseClicked

    private void dir_lb_frame1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dir_lb_frame1MouseClicked

    }//GEN-LAST:event_dir_lb_frame1MouseClicked

    private void dns_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dns_lb_frameMouseClicked

    }//GEN-LAST:event_dns_lb_frameMouseClicked

    private void ssl_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ssl_lb_frameMouseClicked

    }//GEN-LAST:event_ssl_lb_frameMouseClicked

    private void subdomain_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_frameMouseClicked

    }//GEN-LAST:event_subdomain_lb_frameMouseClicked

    private void wpscan_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_frameMouseClicked

    }//GEN-LAST:event_wpscan_lb_frameMouseClicked

    private void tracert_lb_frameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_frameMouseClicked

    }//GEN-LAST:event_tracert_lb_frameMouseClicked

    private void nmap_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_frameMouseEntered
        nmap_lb_frame.setBackground(new Color(255, 255, 255));
        nmap_lb_frame.setForeground(new Color(120, 212, 247));
        nmap_lb_frame.setOpaque(true);
    }//GEN-LAST:event_nmap_lb_frameMouseEntered

    private void nmap_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_frameMouseExited
        if (selectedFrame.equals("nmap")) {

        } else {
            nmap_lb_frame.setBackground(new Color(120, 212, 247));
            nmap_lb_frame.setForeground(new Color(255, 255, 255));
            nmap_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_nmap_lb_frameMouseExited

    private void dir_lb_frame1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dir_lb_frame1MouseEntered
        dir_lb_frame1.setBackground(new Color(255, 255, 255));
        dir_lb_frame1.setForeground(new Color(120, 212, 247));
        dir_lb_frame1.setOpaque(true);
    }//GEN-LAST:event_dir_lb_frame1MouseEntered

    private void dir_lb_frame1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dir_lb_frame1MouseExited
        if (selectedFrame.equals("dir")) {

        } else {
            dir_lb_frame1.setBackground(new Color(120, 212, 247));
            dir_lb_frame1.setForeground(new Color(255, 255, 255));
            dir_lb_frame1.setOpaque(true);
        }
    }//GEN-LAST:event_dir_lb_frame1MouseExited

    private void dns_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dns_lb_frameMouseEntered
        dns_lb_frame.setBackground(new Color(255, 255, 255));
        dns_lb_frame.setForeground(new Color(120, 212, 247));
        dns_lb_frame.setOpaque(true);
    }//GEN-LAST:event_dns_lb_frameMouseEntered

    private void dns_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dns_lb_frameMouseExited
        if (selectedFrame.equals("dns")) {

        } else {
            dns_lb_frame.setBackground(new Color(120, 212, 247));
            dns_lb_frame.setForeground(new Color(255, 255, 255));
            dns_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_dns_lb_frameMouseExited

    private void ssl_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ssl_lb_frameMouseEntered
        ssl_lb_frame.setBackground(new Color(255, 255, 255));
        ssl_lb_frame.setForeground(new Color(120, 212, 247));
        ssl_lb_frame.setOpaque(true);
    }//GEN-LAST:event_ssl_lb_frameMouseEntered

    private void ssl_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ssl_lb_frameMouseExited
        if (selectedFrame.equals("ssl")) {

        } else {
            ssl_lb_frame.setBackground(new Color(120, 212, 247));
            ssl_lb_frame.setForeground(new Color(255, 255, 255));
            ssl_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_ssl_lb_frameMouseExited

    private void subdomain_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_frameMouseEntered
        subdomain_lb_frame.setBackground(new Color(255, 255, 255));
        subdomain_lb_frame.setForeground(new Color(120, 212, 247));
        subdomain_lb_frame.setOpaque(true);
    }//GEN-LAST:event_subdomain_lb_frameMouseEntered

    private void subdomain_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_frameMouseExited
        if (selectedFrame.equals("subdomain")) {

        } else {
            subdomain_lb_frame.setBackground(new Color(120, 212, 247));
            subdomain_lb_frame.setForeground(new Color(255, 255, 255));
            subdomain_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_subdomain_lb_frameMouseExited

    private void wpscan_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_frameMouseEntered
        wpscan_lb_frame.setBackground(new Color(255, 255, 255));
        wpscan_lb_frame.setForeground(new Color(120, 212, 247));
        wpscan_lb_frame.setOpaque(true);
    }//GEN-LAST:event_wpscan_lb_frameMouseEntered

    private void wpscan_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_frameMouseExited
        if (selectedFrame.equals("wpscan")) {

        } else {
            wpscan_lb_frame.setBackground(new Color(120, 212, 247));
            wpscan_lb_frame.setForeground(new Color(255, 255, 255));
            wpscan_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_wpscan_lb_frameMouseExited

    private void tracert_lb_frameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_frameMouseEntered
        tracert_lb_frame.setBackground(new Color(255, 255, 255));
        tracert_lb_frame.setForeground(new Color(120, 212, 247));
        tracert_lb_frame.setOpaque(true);
    }//GEN-LAST:event_tracert_lb_frameMouseEntered

    private void tracert_lb_frameMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_frameMouseExited
        if (selectedFrame.equals("tracert")) {

        } else {
            tracert_lb_frame.setBackground(new Color(120, 212, 247));
            tracert_lb_frame.setForeground(new Color(255, 255, 255));
            tracert_lb_frame.setOpaque(true);
        }
    }//GEN-LAST:event_tracert_lb_frameMouseExited

    private void nmap_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_rbMouseClicked

    }//GEN-LAST:event_nmap_lb_rbMouseClicked

    private void dirscan_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dirscan_lb_rbMouseClicked

    }//GEN-LAST:event_dirscan_lb_rbMouseClicked

    private void dnsrecon_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dnsrecon_lb_rbMouseClicked

    }//GEN-LAST:event_dnsrecon_lb_rbMouseClicked

    private void sslchecker_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sslchecker_lb_rbMouseClicked

    }//GEN-LAST:event_sslchecker_lb_rbMouseClicked

    private void subdomain_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_rbMouseClicked

    }//GEN-LAST:event_subdomain_lb_rbMouseClicked

    private void wpscan_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_rbMouseClicked

    }//GEN-LAST:event_wpscan_lb_rbMouseClicked

    private void tracert_lb_rbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_rbMouseClicked

    }//GEN-LAST:event_tracert_lb_rbMouseClicked

    private void nmap_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_rbMouseEntered
        if (nmap_lb_rb.isEnabled()) {
            nmap_lb_rb.setBackground(new Color(255, 255, 255));
            nmap_lb_rb.setForeground(new Color(0, 156, 207));
            nmap_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) nmap_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            nmap_lb_rb.setBorder(border);
            nmap_lb_rb.repaint();
            nmap_lb_rb.revalidate();
        }
    }//GEN-LAST:event_nmap_lb_rbMouseEntered

    private void nmap_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_rbMouseExited
        nmap_lb_rb.setBackground(new Color(0, 156, 207));
        nmap_lb_rb.setForeground(new Color(255, 255, 255));
        nmap_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) nmap_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        nmap_lb_rb.setBorder(border);
        nmap_lb_rb.repaint();
        nmap_lb_rb.revalidate();
    }//GEN-LAST:event_nmap_lb_rbMouseExited

    private void dirscan_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dirscan_lb_rbMouseEntered
        if (dirscan_lb_rb.isEnabled()) {
            dirscan_lb_rb.setBackground(new Color(255, 255, 255));
            dirscan_lb_rb.setForeground(new Color(0, 156, 207));
            dirscan_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) dirscan_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            dirscan_lb_rb.setBorder(border);
            dirscan_lb_rb.repaint();
            dirscan_lb_rb.revalidate();
        }
    }//GEN-LAST:event_dirscan_lb_rbMouseEntered

    private void dirscan_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dirscan_lb_rbMouseExited
        dirscan_lb_rb.setBackground(new Color(0, 156, 207));
        dirscan_lb_rb.setForeground(new Color(255, 255, 255));
        dirscan_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) dirscan_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        dirscan_lb_rb.setBorder(border);
        dirscan_lb_rb.repaint();
        dirscan_lb_rb.revalidate();
    }//GEN-LAST:event_dirscan_lb_rbMouseExited

    private void dnsrecon_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dnsrecon_lb_rbMouseEntered
        if (dnsrecon_lb_rb.isEnabled()) {
            dnsrecon_lb_rb.setBackground(new Color(255, 255, 255));
            dnsrecon_lb_rb.setForeground(new Color(0, 156, 207));
            dnsrecon_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) dnsrecon_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            dnsrecon_lb_rb.setBorder(border);
            dnsrecon_lb_rb.repaint();
            dnsrecon_lb_rb.revalidate();
        }
    }//GEN-LAST:event_dnsrecon_lb_rbMouseEntered

    private void dnsrecon_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dnsrecon_lb_rbMouseExited
        dnsrecon_lb_rb.setBackground(new Color(0, 156, 207));
        dnsrecon_lb_rb.setForeground(new Color(255, 255, 255));
        dnsrecon_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) dnsrecon_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        dnsrecon_lb_rb.setBorder(border);
        dnsrecon_lb_rb.repaint();
        dnsrecon_lb_rb.revalidate();
    }//GEN-LAST:event_dnsrecon_lb_rbMouseExited

    private void sslchecker_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sslchecker_lb_rbMouseEntered
        if (sslchecker_lb_rb.isEnabled()) {
            sslchecker_lb_rb.setBackground(new Color(255, 255, 255));
            sslchecker_lb_rb.setForeground(new Color(0, 156, 207));
            sslchecker_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) sslchecker_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            sslchecker_lb_rb.setBorder(border);
            sslchecker_lb_rb.repaint();
            sslchecker_lb_rb.revalidate();
        }
    }//GEN-LAST:event_sslchecker_lb_rbMouseEntered

    private void sslchecker_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sslchecker_lb_rbMouseExited
        sslchecker_lb_rb.setBackground(new Color(0, 156, 207));
        sslchecker_lb_rb.setForeground(new Color(255, 255, 255));
        sslchecker_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) sslchecker_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        sslchecker_lb_rb.setBorder(border);
        sslchecker_lb_rb.repaint();
        sslchecker_lb_rb.revalidate();
    }//GEN-LAST:event_sslchecker_lb_rbMouseExited

    private void subdomain_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_rbMouseEntered
        if (subdomain_lb_rb.isEnabled()) {
            subdomain_lb_rb.setBackground(new Color(255, 255, 255));
            subdomain_lb_rb.setForeground(new Color(0, 156, 207));
            subdomain_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) subdomain_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            subdomain_lb_rb.setBorder(border);
            subdomain_lb_rb.repaint();
            subdomain_lb_rb.revalidate();
        }
    }//GEN-LAST:event_subdomain_lb_rbMouseEntered

    private void subdomain_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_rbMouseExited
        subdomain_lb_rb.setBackground(new Color(0, 156, 207));
        subdomain_lb_rb.setForeground(new Color(255, 255, 255));
        subdomain_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) subdomain_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        subdomain_lb_rb.setBorder(border);
        subdomain_lb_rb.repaint();
        subdomain_lb_rb.revalidate();
    }//GEN-LAST:event_subdomain_lb_rbMouseExited

    private void wpscan_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_rbMouseEntered
        if (wpscan_lb_rb.isEnabled()) {
            wpscan_lb_rb.setBackground(new Color(255, 255, 255));
            wpscan_lb_rb.setForeground(new Color(0, 156, 207));
            wpscan_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) wpscan_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            wpscan_lb_rb.setBorder(border);
            wpscan_lb_rb.repaint();
            wpscan_lb_rb.revalidate();
        }
    }//GEN-LAST:event_wpscan_lb_rbMouseEntered

    private void wpscan_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_rbMouseExited
        wpscan_lb_rb.setBackground(new Color(0, 156, 207));
        wpscan_lb_rb.setForeground(new Color(255, 255, 255));
        wpscan_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) wpscan_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        wpscan_lb_rb.setBorder(border);
        wpscan_lb_rb.repaint();
        wpscan_lb_rb.revalidate();
    }//GEN-LAST:event_wpscan_lb_rbMouseExited

    private void tracert_lb_rbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_rbMouseEntered
        if (tracert_lb_rb.isEnabled()) {
            tracert_lb_rb.setBackground(new Color(255, 255, 255));
            tracert_lb_rb.setForeground(new Color(0, 156, 207));
            tracert_lb_rb.setOpaque(true);
            TitledBorder border = (TitledBorder) tracert_lb_rb.getBorder();
            border.setBorder(new EtchedBorder(new Color(0, 156, 207), new Color(0, 156, 207)));
            tracert_lb_rb.setBorder(border);
            tracert_lb_rb.repaint();
            tracert_lb_rb.revalidate();
        }
    }//GEN-LAST:event_tracert_lb_rbMouseEntered

    private void tracert_lb_rbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_rbMouseExited
        tracert_lb_rb.setBackground(new Color(0, 156, 207));
        tracert_lb_rb.setForeground(new Color(255, 255, 255));
        tracert_lb_rb.setOpaque(true);
        TitledBorder border = (TitledBorder) tracert_lb_rb.getBorder();
        border.setBorder(new EtchedBorder(new Color(255, 255, 255), new Color(255, 255, 255)));
        tracert_lb_rb.setBorder(border);
        tracert_lb_rb.repaint();
        tracert_lb_rb.revalidate();
    }//GEN-LAST:event_tracert_lb_rbMouseExited

    private void hide_labelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hide_labelMouseClicked
        ScanFrame.setVisibleFrame(false);
    }//GEN-LAST:event_hide_labelMouseClicked

    private void hide_labelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hide_labelMouseEntered
        hide_label.setBackground(new Color(255, 255, 255));
        hide_label.setForeground(new Color(83, 86, 90));
        hide_label.setOpaque(true);
    }//GEN-LAST:event_hide_labelMouseEntered

    private void hide_labelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hide_labelMouseExited
        hide_label.setBackground(new Color(83, 86, 90));
        hide_label.setForeground(new Color(255, 255, 255));
        hide_label.setOpaque(true);
    }//GEN-LAST:event_hide_labelMouseExited

    private void hide_labelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hide_labelMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_hide_labelMousePressed

    private void nmap_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_rbMouseReleased
        if (nmap) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) nmap_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            nmap_lb_rb.setBorder(border);
            nmap_lb_rb.repaint();
            nmap_lb_rb.revalidate();
            nmap = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) nmap_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            nmap_lb_rb.setBorder(border);
            nmap_lb_rb.repaint();
            nmap_lb_rb.revalidate();
            nmap = true;
        }
    }//GEN-LAST:event_nmap_lb_rbMouseReleased

    private void nmap_lb_rbMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_rbMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_nmap_lb_rbMousePressed

    private void dirscan_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dirscan_lb_rbMouseReleased
        if (dirscan) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) dirscan_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            dirscan_lb_rb.setBorder(border);
            dirscan_lb_rb.repaint();
            dirscan_lb_rb.revalidate();
            dirscan = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) dirscan_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            dirscan_lb_rb.setBorder(border);
            dirscan_lb_rb.repaint();
            dirscan_lb_rb.revalidate();
            dirscan = true;
        }
    }//GEN-LAST:event_dirscan_lb_rbMouseReleased

    private void dnsrecon_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dnsrecon_lb_rbMouseReleased
        if (dnsrecon) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) dnsrecon_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            dnsrecon_lb_rb.setBorder(border);
            dnsrecon_lb_rb.repaint();
            dnsrecon_lb_rb.revalidate();
            dnsrecon = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) dnsrecon_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            dnsrecon_lb_rb.setBorder(border);
            dnsrecon_lb_rb.repaint();
            dnsrecon_lb_rb.revalidate();
            dnsrecon = true;
        }
    }//GEN-LAST:event_dnsrecon_lb_rbMouseReleased

    private void sslchecker_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sslchecker_lb_rbMouseReleased
        if (sslchecker) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) sslchecker_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            sslchecker_lb_rb.setBorder(border);
            sslchecker_lb_rb.repaint();
            sslchecker_lb_rb.revalidate();
            sslchecker = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) sslchecker_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            sslchecker_lb_rb.setBorder(border);
            sslchecker_lb_rb.repaint();
            sslchecker_lb_rb.revalidate();
            sslchecker = true;
        }
    }//GEN-LAST:event_sslchecker_lb_rbMouseReleased

    private void subdomain_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_rbMouseReleased
        if (subdomainscan) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) subdomain_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            subdomain_lb_rb.setBorder(border);
            subdomain_lb_rb.repaint();
            subdomain_lb_rb.revalidate();
            subdomainscan = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) subdomain_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            subdomain_lb_rb.setBorder(border);
            subdomain_lb_rb.repaint();
            subdomain_lb_rb.revalidate();
            subdomainscan = true;
        }
    }//GEN-LAST:event_subdomain_lb_rbMouseReleased

    private void wpscan_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_rbMouseReleased
        if (wpscan) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) wpscan_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            wpscan_lb_rb.setBorder(border);
            wpscan_lb_rb.repaint();
            wpscan_lb_rb.revalidate();
            wpscan = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) wpscan_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            wpscan_lb_rb.setBorder(border);
            wpscan_lb_rb.repaint();
            wpscan_lb_rb.revalidate();
            wpscan = true;
        }
    }//GEN-LAST:event_wpscan_lb_rbMouseReleased

    private void tracert_lb_rbMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_rbMouseReleased
        if (tracert) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/power_down.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) tracert_lb_rb.getBorder();
            border.setTitle("OFF");
            border.setTitleColor(new Color(255, 51, 51));
            tracert_lb_rb.setBorder(border);
            tracert_lb_rb.repaint();
            tracert_lb_rb.revalidate();
            tracert = false;
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/activated.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            TitledBorder border = (TitledBorder) tracert_lb_rb.getBorder();
            border.setTitle("ON");
            border.setTitleColor(new Color(0, 204, 102));
            tracert_lb_rb.setBorder(border);
            tracert_lb_rb.repaint();
            tracert_lb_rb.revalidate();
            tracert = true;
        }
    }//GEN-LAST:event_tracert_lb_rbMouseReleased

    private void nmap_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nmap_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(nmap_panel1);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "nmap";
        nmap_lb_frame.setBackground(new Color(255, 255, 255));
        nmap_lb_frame.setForeground(new Color(120, 212, 247));
        nmap_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_nmap_lb_frameMouseReleased

    private void dir_lb_frame1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dir_lb_frame1MouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(dir_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "dir";
        dir_lb_frame1.setBackground(new Color(255, 255, 255));
        dir_lb_frame1.setForeground(new Color(120, 212, 247));
        dir_lb_frame1.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_dir_lb_frame1MouseReleased

    private void dns_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dns_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(dnsrecon_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "dns";
        dns_lb_frame.setBackground(new Color(255, 255, 255));
        dns_lb_frame.setForeground(new Color(120, 212, 247));
        dns_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_dns_lb_frameMouseReleased

    private void ssl_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ssl_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(ssl_checker_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "ssl";
        ssl_lb_frame.setBackground(new Color(255, 255, 255));
        ssl_lb_frame.setForeground(new Color(120, 212, 247));
        ssl_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_ssl_lb_frameMouseReleased

    private void subdomain_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subdomain_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(subdomain_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "subdomain";
        subdomain_lb_frame.setBackground(new Color(255, 255, 255));
        subdomain_lb_frame.setForeground(new Color(120, 212, 247));
        subdomain_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_subdomain_lb_frameMouseReleased

    private void wpscan_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wpscan_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(wpscan_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "wpscan";
        wpscan_lb_frame.setBackground(new Color(255, 255, 255));
        wpscan_lb_frame.setForeground(new Color(120, 212, 247));
        wpscan_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_wpscan_lb_frameMouseReleased

    private void tracert_lb_frameMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tracert_lb_frameMouseReleased
        result_lay_panel.removeAll();
        result_lay_panel.add(tracert_panel);
        result_lay_panel.repaint();
        result_lay_panel.revalidate();
        selectedFrame = "tracert";
        tracert_lb_frame.setBackground(new Color(255, 255, 255));
        tracert_lb_frame.setForeground(new Color(120, 212, 247));
        tracert_lb_frame.setOpaque(true);
        changeSelectionColor(selectedFrame);
    }//GEN-LAST:event_tracert_lb_frameMouseReleased

    private void restart_labelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_restart_labelMouseClicked
        int yes_no = JOptionPane.showConfirmDialog(this, "Do you want to Restart ?", "Confirm", JOptionPane.YES_NO_OPTION);
        if (yes_no == JOptionPane.YES_OPTION) {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AudioPlayer player = new AudioPlayer();
                        player.play(ScanFrame.class.getResource("sound/calibrating.wav").getPath());
                    } catch (Exception ex) {
                    }
                }
            });
            th.start();
            
            scanFrame.setVisible(false);
            scanFrame.dispose();
            scanFrame = new ScanFrame();
            scanFrame.setVisible(true);
        }
    }//GEN-LAST:event_restart_labelMouseClicked

    private void restart_labelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_restart_labelMouseEntered
        restart_label.setBackground(new Color(255, 255, 255));
        restart_label.setForeground(new Color(83, 86, 90));
        restart_label.setOpaque(true);
    }//GEN-LAST:event_restart_labelMouseEntered

    private void restart_labelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_restart_labelMouseExited
        restart_label.setBackground(new Color(83, 86, 90));
        restart_label.setForeground(new Color(255, 255, 255));
        restart_label.setOpaque(true);
    }//GEN-LAST:event_restart_labelMouseExited

    private void restart_labelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_restart_labelMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_restart_labelMousePressed

    private static void changeSelectionColor(String name) {
        if (!name.equals("nmap")) {
            nmap_lb_frame.setBackground(new Color(120, 212, 247));
            nmap_lb_frame.setForeground(new Color(255, 255, 255));
            nmap_lb_frame.setOpaque(true);
        }
        if (!name.equals("dir")) {
            dir_lb_frame1.setBackground(new Color(120, 212, 247));
            dir_lb_frame1.setForeground(new Color(255, 255, 255));
            dir_lb_frame1.setOpaque(true);
        }
        if (!name.equals("dns")) {
            dns_lb_frame.setBackground(new Color(120, 212, 247));
            dns_lb_frame.setForeground(new Color(255, 255, 255));
            dns_lb_frame.setOpaque(true);
        }
        if (!name.equals("ssl")) {
            ssl_lb_frame.setBackground(new Color(120, 212, 247));
            ssl_lb_frame.setForeground(new Color(255, 255, 255));
            ssl_lb_frame.setOpaque(true);
        }
        if (!name.equals("subdomain")) {
            subdomain_lb_frame.setBackground(new Color(120, 212, 247));
            subdomain_lb_frame.setForeground(new Color(255, 255, 255));
            subdomain_lb_frame.setOpaque(true);
        }
        if (!name.equals("wpscan")) {
            wpscan_lb_frame.setBackground(new Color(120, 212, 247));
            wpscan_lb_frame.setForeground(new Color(255, 255, 255));
            wpscan_lb_frame.setOpaque(true);
        }
        if (!name.equals("tracert")) {
            tracert_lb_frame.setBackground(new Color(120, 212, 247));
            tracert_lb_frame.setForeground(new Color(255, 255, 255));
            tracert_lb_frame.setOpaque(true);
        }
    }

    public static String getGlobalIpAdd() {
        String ip = "";
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));

            ip = in.readLine();
        } catch (Exception ex) {

        }
        return ip;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel dir_lb_frame;
    private static javax.swing.JLabel dir_lb_frame1;
    private javax.swing.JPanel dir_panel;
    public static javax.swing.JTextArea dir_text_area;
    public static javax.swing.JTree dir_tree;
    private static javax.swing.JLabel dirscan_lb_rb;
    private static javax.swing.JLabel dns_lb_frame;
    public static javax.swing.JTextArea dns_recon_text_area;
    private static javax.swing.JLabel dnsrecon_lb_rb;
    private javax.swing.JPanel dnsrecon_panel;
    public static javax.swing.JTextField domain_scan_frame_tf;
    private javax.swing.JLabel exit_label;
    private javax.swing.JLabel hide_label;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private static javax.swing.JTable nmapTable;
    private static javax.swing.JLabel nmap_lb_frame;
    private static javax.swing.JLabel nmap_lb_rb;
    private javax.swing.JPanel nmap_panel1;
    public static javax.swing.JTextArea nmap_text_area1;
    public static javax.swing.JLabel pause_lb;
    private javax.swing.JLabel restart_label;
    private javax.swing.JLayeredPane result_lay_panel;
    public static javax.swing.JLabel resume_lb;
    public static javax.swing.JLabel sap_label;
    public static javax.swing.JLabel save_lb;
    private javax.swing.JPanel ssl_checker_panel;
    public static javax.swing.JTextArea ssl_checker_text_area;
    private static javax.swing.JLabel ssl_lb_frame;
    private static javax.swing.JLabel sslchecker_lb_rb;
    public static javax.swing.JLabel stop_lb;
    private static javax.swing.JTable subdomainTable;
    private static javax.swing.JLabel subdomain_lb_frame;
    private static javax.swing.JLabel subdomain_lb_rb;
    private javax.swing.JPanel subdomain_panel;
    public static javax.swing.JTextArea subdomain_text_area;
    private static javax.swing.JLabel tracert_lb_frame;
    private static javax.swing.JLabel tracert_lb_rb;
    private javax.swing.JPanel tracert_panel;
    public static javax.swing.JTextArea tracert_text_area;
    private static javax.swing.JLabel wpscan_lb_frame;
    private static javax.swing.JLabel wpscan_lb_rb;
    private javax.swing.JPanel wpscan_panel;
    public static javax.swing.JTextArea wpscan_text_area;
    // End of variables declaration//GEN-END:variables
}
