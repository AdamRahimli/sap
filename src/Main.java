
import java.awt.Frame;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

// Scan All Possibility (SAP)
// @author B.A.
public class Main {

    static TrayIcon trayIcon;
    private static Properties prop = new Properties();

    public static void loadConfig() {
        try ( FileInputStream input = new FileInputStream("config.properties")) {
            prop.load(input);
            //System.out.println(prop.getProperty("nmap.query"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /* 
    public static void main(String[] args) {
        
        try (FileInputStream input = new FileInputStream("config.properties")) {
            prop.load(input);
            //System.out.println(prop.getProperty("nmap.query"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        //MainFrame mainFrame = new MainFrame();
        //mainFrame.setVisible(true);
        
        ScanFrame scanFrame = new ScanFrame();
        //scanFrame.setVisible(true);
        
        if (!scanFrame.isUndecorated()) {
            scanFrame.setUndecorated(true);
        }
        if(!scanFrame.isVisible()){
            scanFrame.setVisible(true);
        }
        scanFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
        
        
        //runWPSCAN("--help");
        
    }*/
    public static void runNMAP(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("nmap.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.nmap_text_area1.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.nmap_text_area1.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public static boolean checkTCPport(int port,String addres){
        boolean result = false;
        try{
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(addres,port),1000);
            socket.close();
            result = true;
            System.out.println(port + " is open");
        }catch(Exception ex){
            result = false;
        }
        return result;
    }
    
    public static boolean checkUDPport(int port){
        boolean result = false;
        
        return result;
    }

    public static void runDIR_SCAN(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("dirscan.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.dir_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.dir_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runDNSRECON(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("dnsrecon.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.dns_recon_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.dns_recon_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runSSL_CHECKER(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("sslchecker.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.ssl_checker_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.ssl_checker_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runSUBDOMAIN_SCAN(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("subdomain.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.subdomain_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.subdomain_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runWPSCAN(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("wpscan.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.wpscan_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.wpscan_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runTRACERT(String target) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", prop.getProperty("tracert.query") + " " + target);
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                ScanFrame.tracert_text_area.append(line + "\n");
                //System.out.println(line);
            }
            int exitCode = process.waitFor();
            ScanFrame.tracert_text_area.append("\nExited with error code : " + exitCode + "\n");
            //System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getDomainName(String url) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;
        } catch (Exception ex) {
            return null;
        }
    }

    public static void updateWPSCAN() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", "cd " + System.getProperty("user.dir") + "\\..\\wpscan & wpscan --update --disable-tls-checks");
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                //ScanFrame.nmap_text_area.append(line+"\n");
                System.out.println(line);
            }
            int exitCode = process.waitFor();
            //ScanFrame.nmap_text_area.append("\nExited with error code : "+exitCode+"\n");
            System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void installWPSCAN() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("cmd.exe", "/c", "cd " + System.getProperty("user.dir") + "\\..\\wpscan & gem install wpscan");
        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                //ScanFrame.nmap_text_area.append(line+"\n");
                System.out.println(line);
            }
            int exitCode = process.waitFor();
            //ScanFrame.nmap_text_area.append("\nExited with error code : "+exitCode+"\n");
            System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void show() {
        if (!SystemTray.isSupported()) {
            System.exit(0);
        }
        trayIcon = new TrayIcon(createIcon("/img/icon_win.png", "icon"));
        trayIcon.setToolTip("S.A.P.");
        final SystemTray tray = SystemTray.getSystemTray();

        final PopupMenu menu = new PopupMenu();

        MenuItem open = new MenuItem("Open");
        MenuItem exit = new MenuItem("Exit");

        menu.add(open);
        menu.addSeparator();
        menu.add(exit);

        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ScanFrame.setVisibleFrame(true);
            }
        });
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        trayIcon.setPopupMenu(menu);
        try {
            tray.add(trayIcon);
        } catch (Exception ex) {

        }
    }

    protected static Image createIcon(String path, String desc) {
        URL imageUrl = Main.class.getResource(path);
        return (new ImageIcon(imageUrl, desc)).getImage();
    }

    public static Set<String> googleSearchResult(String qry) {
        Set<String> result = new HashSet<String>();
        try {
            String user_agent[] = {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15",
	"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.60 YaBrowser/20.12.0.963 Yowser/2.5 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Safari/605.1.15",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"};
            String key = "AIzaSyA6RGoQdVfJj9zNdCvdBYfuVKqQKD7nWB4";
            int start = 1;
            boolean trigger = false;
            while (true) {
                try{
                    Thread.sleep(1000+(int)(Math.random()*1500));
                }catch(Exception ex){
                    
                }
                URL url = new URL(
                        "https://www.googleapis.com/customsearch/v1?key=" + key + "&cx=013036536707430787589:_pqjad5hr1a&q=" + qry + "&alt=json&start=" + start);
                try {
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("User-Agent", user_agent[(int)(Math.random()*12)-1]);
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

                    String output;
                    System.out.println("Output from Server .... \n");
                    while ((output = br.readLine()) != null) {

                        if (output.contains("\"link\": \"")) {
                            String link = output.substring(output.indexOf("\"link\": \"") + ("\"link\": \"").length(), output.indexOf("\","));
                            System.out.println(link);       //Will print the google search links
                            result.add(link);
                        }
                    }
                    conn.disconnect();
                    if(trigger){
                        break;
                    }
                    start += 10;
                }catch(Exception ex){
                    start -= 1;
                    trigger = true;
                }
            }
        } catch (Exception ex) {

        }
        return result;
    }
}
