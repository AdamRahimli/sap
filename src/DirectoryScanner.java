
import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ntro
 */
public class DirectoryScanner {
    
    public static ResponseClass requestUrl(String domain,String path){
        ResponseClass responseClass = new ResponseClass();
        responseClass.setDomain(domain);
        responseClass.setPath(path);

        StringBuffer content = new StringBuffer();
        try{
            URL url = new URL(domain+"/"+path);
            responseClass.setUrl(url);
            if(url.getProtocol().equalsIgnoreCase("http")){
                HttpURLConnection con = null;
                con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                int statusCode = con.getResponseCode();
                con.setInstanceFollowRedirects(false);
                responseClass.setCode(statusCode);
                try{
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine + "\n");
                    }
                }catch(Exception ex){

                }
                responseClass.setContent(content.toString());
                responseClass.setSize(content.toString().length());
            }else if(url.getProtocol().equalsIgnoreCase("https")){
                HttpsURLConnection con = null;
                con = (HttpsURLConnection) url.openConnection();
                con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                int statusCode = con.getResponseCode();
                con.setInstanceFollowRedirects(false);
                responseClass.setCode(statusCode);
                try{
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine + "\n");
                    }
                }catch(Exception ex){

                }
                responseClass.setContent(content.toString());
                responseClass.setSize(content.toString().length());
            }
        }catch(Exception ex){
            
        }
        return responseClass;
    }
    
    
}
